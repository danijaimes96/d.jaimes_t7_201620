package taller.test;

import java.io.IOException;
import java.util.ArrayList;

import junit.framework.TestCase;
import taller.interfaz.ArbolBinario;

public class MainTest extends TestCase
{
	private ArrayList<String> lista = new ArrayList<>();
	private ArrayList<String> lista2 = new ArrayList<>();
	private ArbolBinario arbolito = new ArbolBinario();
	private ArbolBinario arbolito2 = new ArbolBinario();
	private ArbolBinario arbolito3 = new ArbolBinario();

	public void setUp1() throws IOException
	{
		arbolito.cargarArchivo("./data/ejemplotest.properties");
		arbolito.reconstruir();
		lista.add("d");
		lista.add("b");
		lista.add("e");
		lista.add("a");
		lista.add("f");
		lista.add("c");
		lista.add("g");
		lista2.add("a");
		lista2.add("b");
		lista2.add("d");
		lista2.add("e");
		lista2.add("c");
		lista2.add("f");
		lista2.add("g");
		arbolito2.cargarArchivo("./data/ejemplotest3.properties");
		arbolito2.reconstruir();
		arbolito3.cargarArchivo("./data/ejemplotest2.properties");
		arbolito3.reconstruir();
	}

	public void setUp2() throws IOException
	{
		arbolito.cargarArchivo("./data/ejemplotest2.properties");
		arbolito.reconstruir();
		lista.add("k");
		lista.add("l");
		lista.add("j");
		lista.add("h");
		lista.add("q");
		lista.add("o");
		lista.add("p");

		lista2.add("j");
		lista2.add("k");
		lista2.add("l");
		lista2.add("o");
		lista2.add("q");
		lista2.add("h");
		lista2.add("p");

		arbolito2.cargarArchivo("./data/ejemplotest3.properties");
		arbolito2.reconstruir();
		arbolito3.cargarArchivo("./data/ejemplotest2.properties");
		arbolito3.reconstruir();
	}

	public void testCaso1In() throws IOException
	{
		setUp1();

		assertEquals(lista, arbolito.listaIn());

	}

	public void testCaso1Pre() throws IOException
	{
		setUp1();

		assertEquals(lista2, arbolito.listaPre());

	}

	public void testCaso1Sub() throws IOException
	{
		setUp1();

		assertEquals(true, arbolito.subArbol(arbolito2));

		assertEquals(false, arbolito.subArbol(arbolito3));

	}

	public void testCaso2In() throws IOException
	{
		setUp2();

		assertEquals(lista, arbolito.listaIn());

	}

	public void testCaso2Pre() throws IOException
	{
		setUp2();

		assertEquals(lista2, arbolito.listaPre());

	}

	public void testCaso2Sub() throws IOException
	{
		setUp2();

		assertEquals(false, arbolito.subArbol(arbolito2));

		assertEquals(true, arbolito.subArbol(arbolito3));
	}

}