package taller.interfaz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

import org.json.JSONObject;

public class ArbolBinario implements IReconstructorArbol
{
	private String[] arreglitoPre;
	private String[] arreglitoIn;
	private NodoArbol root;

	public class NodoArbol
	{
		private String valor;
		private NodoArbol izq;
		private NodoArbol der;

		public NodoArbol(String valor)
		{
			super();
			izq = null;
			der = null;
			this.valor = valor;
		}

		public String getValor()
		{
			return valor;
		}

		public void setValor(String valor)
		{
			this.valor = valor;
		}

		public NodoArbol getIzq()
		{
			return izq;
		}

		public void setIzq(NodoArbol izq)
		{
			this.izq = izq;
		}

		public NodoArbol getDer()
		{
			return der;
		}

		public void setDer(NodoArbol der)
		{
			this.der = der;
		}

		public void construir(String aGuardar)
		{
			if (before(aGuardar, valor))
			{
				if (izq == null)
				{
					setIzq(new NodoArbol(aGuardar));
				}
				else
				{
					izq.construir(aGuardar);
				}
			}
			else
			{
				if (der == null)
				{
					setDer(new NodoArbol(aGuardar));
				}
				else
				{
					der.construir(aGuardar);
				}
			}
		}

		public String toString(String tab, String pre)
		{
			StringBuilder sBuilder = new StringBuilder();

			sBuilder.append(tab + pre + valor + System.lineSeparator());

			tab = tab.replace("├", "│");
			tab = tab.replace("└", " ");
			pre = pre.replaceAll("L: ", "");
			pre = pre.replaceAll("R: ", "");

			tab += "     " + new String(new char[valor.toString().length() / 2])
					.replace('\0', ' ');

			if (izq != null)
			{
				String t2 = "└";
				if (der != null)
				{
					t2 = "├";
				}
				sBuilder.append(izq.toString(tab + t2, pre + "L: "));
			}
			if (der != null)
			{
				sBuilder.append(der.toString(tab + "└", pre + "R: "));
			}
			return sBuilder.toString();
		}

		public NodoArbol lookFor(String look)
		{
			NodoArbol n = null;
			if (valor.equals(look))
			{
				n = this;
			}
			else
			{
				if (izq != null)
				{
					n = izq.lookFor(look);
				}
				if (n == null && der != null)
				{
					n = der.lookFor(look);
				}
			}
			return n;
		}

		public boolean check(NodoArbol sub)
		{
			boolean rta = false;
			rta = sub.getValor().equals(valor);
			if (rta)
			{
				if (izq != null && sub.izq != null)
				{
					rta &= izq.check(sub.izq);
				}
				else if (izq == null && sub.izq == null)
				{
					rta = true;
				}
				else
				{
					rta = false;
				}
				if (rta && der != null && sub.der != null)
				{
					rta &= der.check(sub.der);
				}
				else if (der == null && sub.der == null)
				{
					rta = true;
				}
				else
				{
					rta = false;
				}
			}

			return rta;
		}
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException
	{
		// TODO Auto-generated method stub
		FileInputStream in = new FileInputStream(nombre);
		Properties p = new Properties();
		p.load(in);
		in.close();

		arreglitoPre = p.getProperty("preorden").split(",");
		arreglitoIn = p.getProperty("inorden").split(",");
	}

	@Override
	public void crearArchivo(String info)
			throws FileNotFoundException, UnsupportedEncodingException
	{
		// TODO Auto-generated method stub
		File fileJson = new File(info);
		if (!fileJson.exists())
		{
			try
			{
				fileJson.createNewFile();
			}
			catch (Exception e)
			{
				System.err.println("No se pudo crear el archivo");
			}
		}

		PrintWriter pw = new PrintWriter(fileJson);
		pw.write(new JSONObject(root).toString(4));
		pw.close();

	}

	private boolean before(String aBuscar, String anterior)
	{
		boolean respuesta = false;
		for (int i = 0; i < arreglitoIn.length; i++)
		{
			String actual = arreglitoIn[i];
			if (actual.equals(aBuscar))
			{
				respuesta = true;
				break;
			}
			else if (actual.equals(anterior))

			{
				break;
			}
		}
		return respuesta;
	}

	@Override
	public void reconstruir()
	{
		root = new NodoArbol(arreglitoPre[0]);
		String guardar = arreglitoPre[1];
		for (int i = 1; i < arreglitoIn.length; i++)
		{
			guardar = arreglitoPre[i];
			root.construir(guardar);
		}
	}

	public boolean subArbol(ArbolBinario arbolito)
	{
		boolean respuesta = false;

		if (root != null)
		{
			NodoArbol nodito = root.lookFor(arbolito.root.getValor());
			if (nodito != null)
			{

				respuesta = arbolito.root.check(nodito);
			}
		}
		return respuesta;
	}
	public ArrayList<String> listaIn()
	{	
		ArrayList<String> respuesta = new ArrayList<>();
		for (String n : arreglitoIn)
		{
			respuesta.add(n);
		}
		return respuesta;
		
	}
	public ArrayList<String> listaPre()
	{	
		ArrayList<String> respuesta = new ArrayList<>();
		for (String n : arreglitoPre)
		{
			respuesta.add(n);
		}
		return respuesta;
		
	}
//	@Override
//	public String toString()
//	{
//		return root != null ? root.toString("", "─────") : "--";
//	}
//
//	public static void main(String[] args) throws IOException
//	{
//		ArbolBinario arbolito = new ArbolBinario();
//		arbolito.cargarArchivo("./data/ejemplo.properties");
//		arbolito.reconstruir();
//		
//		ArbolBinario arbolito2 = new ArbolBinario();
//		arbolito2.cargarArchivo("./data/ejemplo2.properties");
//		arbolito2.reconstruir();
//		
//		System.out.println(arbolito.subArbol(arbolito2));
//		System.out.println(arbolito.listaIn());
//	}
}
